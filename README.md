# simp(l)e_touch_mediaplayer

A very simple media-player for touch-interfaces. It's
barely optimized. Use it with at least two
1.5 GHz-cores.

## Usage
Start with "./sitomp /path/to/my/audio-data". Exit by
clicking the upper right of the screen. Pause by
clicking the upper left of the screen.

## Building
Install libSDL2-dev libturbojpeg-dev libSDL2_ttf-dev
libmpg123-dev libogg-dev libvorbis-dev.

    cd simpe_touch_mediaplayer
    mkdir build
    cd build
    cmake ..
    make