#include <list>
#include <map>
#include <set>
#include <string>
#include <iostream>
#include <cstring>
#include <atomic>
#include <thread>
#include <string>
#include <cstdint>

#include <SDL2/SDL.h>
#include <SDL2/SDL_video.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <turbojpeg.h>
#include <mpg123.h>
#include <ogg/ogg.h>
#include <vorbis/codec.h>
#include <vorbis/vorbisfile.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h> 
#include <fcntl.h>
#include <dirent.h>

SDL_Window *main_window;
std::map<std::string, SDL_Texture *> path_to_texture_map;
std::list<std::string> playlist;
std::list<std::string>::iterator playlist_iterator;
SDL_Renderer *renderer;
std::string starting_path;
SDL_AudioSpec want, have;
volatile unsigned char * volatile audio_buffer;
long buffer_length;
//ogg_int16_t convbuffer[4096]; /* take 8k out of the data segment, not the stack */
//int convsize=4096;
//size_t decoded;
//std::atomic<size_t> finished_at;
std::atomic<long> iterator = 0;
//std::atomic<bool> playing = false;
std::atomic<bool> enqueue_next = false;
std::atomic<double> percent_done = 0;
std::atomic<mpg123_handle *> test = NULL;
std::thread * mpg_decode;
std::thread * ogg_decode;
std::atomic<bool> stop_thread = false;
size_t decode_offset = 0;
std::string my_path;
SDL_DisplayMode current;
std::atomic<bool> ogg_thread_deletable = false;
std::atomic<bool> mp3_thread_deletable = false;
std::set<std::string> skip_set{".", "Folder.jpg", "AlbumArtSmall.jpg"};


#define INCREMENT (4*4096*16)

void stopPlaying();

void AudioCallback(void* userdata, unsigned char* stream, int len)
{
    if(iterator + len > buffer_length)
    {
        long till_finished = buffer_length - iterator;
        std::memcpy(stream, (void*) (audio_buffer+iterator), till_finished);
        iterator += till_finished;
        std::memset((void*)(stream+till_finished+1), 0, len-till_finished-1);
    }
    else
    {
        std::memcpy(stream, (void*) (audio_buffer+iterator), len);
        iterator += len;
    }
	percent_done = (double) iterator / (double) buffer_length;
	if(iterator >= buffer_length)
	{
		//stopPlaying();
		enqueue_next = true;
		percent_done = 0;
	}
}

void decodeMP3_thread()
{
    mp3_thread_deletable = false;
	size_t bytes = 1;
	bool flag = false;
	decode_offset =0;
	while(!stop_thread && bytes > 0 && audio_buffer != NULL)
	{
		off_t offset;
		unsigned char * audio;
		mpg123_decode_frame(test, &offset, &audio, &bytes);
		if(decode_offset+bytes > buffer_length)
		{
		    bytes = buffer_length - decode_offset;
		}
		std::memcpy((void*)(audio_buffer + decode_offset), audio, bytes);
		decode_offset += bytes;
		if(!flag)
		{
			SDL_PauseAudio(0);
			flag = true;
		}
	}
	mpg123_close(test);
	mpg123_delete(test);
	mpg123_exit();
	mp3_thread_deletable = true;
}

void decodeOGG_thread(const char * path)
{
    ogg_thread_deletable = false;
    bool flag = false;
    decode_offset = 0;
    char pcmout[4096];
    OggVorbis_File vf;vorbis_info *vi;
    int eof=0;
    int current_section;
    FILE *file = fopen(path, "r");
    if(ov_open_callbacks((void*)file, &vf, NULL, 0, OV_CALLBACKS_NOCLOSE) < 0) {
        fprintf(stderr,"Input does not appear to be an Ogg bitstream.\n");
        exit(1);
    }
    {
        char **ptr=ov_comment(&vf,-1)->user_comments;
        vi=ov_info(&vf,-1);
        while(*ptr){
            fprintf(stderr,"%s\n",*ptr);
            ++ptr;
        }
        fprintf(stderr,"\nBitstream is %d channel, %ldHz\n",vi->channels,vi->rate);
        fprintf(stderr,"\nDecoded length: %ld samples\n",
                (long)ov_pcm_total(&vf,-1));
        long pcm_total = 1;
        long max_pcm = 0;
        int stream_max_pcm = 0;
        int i = 0;
        while((pcm_total = (long)ov_pcm_total(&vf,i)) != OV_EINVAL)
        {
            if(pcm_total > max_pcm)
            {
                stream_max_pcm = i;
                max_pcm = pcm_total;
            };
            std::cerr << i << ": " << pcm_total << std::endl;
            i++;
        }
        current_section = stream_max_pcm;
        buffer_length = (long)ov_pcm_total(&vf,stream_max_pcm) * 4 * vi->channels; // float is 4 byte
        audio_buffer = new unsigned char[buffer_length];
        fprintf(stderr,"Encoded by: %s\n\n",ov_comment(&vf,-1)->vendor);
    }
    while(!eof && !stop_thread){
        long ret=ov_read(&vf,pcmout,sizeof(pcmout),0,2,1,&current_section);
        if (ret == 0) {
            /* EOF */
            eof=1;
        } else if (ret < 0) {
            /* error in the stream.  Not a problem, just reporting it in
           case we (the app) cares.  In this case, we don't. */
            SDL_Log("!!");
        } else {
            /* we don't bother dealing with sample rate changes, etc, but
           you'll have to*/
            std::cerr << (int*) (audio_buffer + 0) << " " << (int*)(audio_buffer + decode_offset) << std::endl;
            for(int i = 0; i < ret/2; i++)
            {
                float * ab_pos = (float *)(audio_buffer + 4 * i + decode_offset);
                *ab_pos = (*(short*)(pcmout + 2*i)) / (float) std::numeric_limits<std::int16_t>::max();
            }
            //std::memcpy((void *)(audio_buffer + decode_offset), pcmout, ret);
            decode_offset += 2*ret;
            if(!flag && decode_offset > 8192)
            {
                want.format = AUDIO_F32;
                want.freq = vi->rate;
                want.channels = vi->channels;

                want.samples = 4096;
                want.callback = &AudioCallback;

                if (SDL_OpenAudio(&want, &have) < 0)
                {
                    SDL_Log("Failed to open audio: %s", SDL_GetError());
                }
                else
                {
                    std::cout << have.freq << "Hz " << (int) have.channels << " channels " << have.samples << " samples" << std::endl;
                }
                iterator = 0;
                SDL_PauseAudio(0);
                flag = true;
            }
        }
    }
    ov_clear(&vf);

    fprintf(stderr,"Done.\n");
    fclose(file);
    ogg_thread_deletable = true;
}

void stopPlaying()
{
	stop_thread = true;
	SDL_PauseAudio(1);
	if(audio_buffer != NULL)
	{
		delete[] audio_buffer;
		audio_buffer = NULL;
	}
	iterator = 0;
	SDL_CloseAudio();
}

void startPlaying(std::string path);

SDL_Renderer * initSDL()
{
	// init SDL
    if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO|SDL_INIT_EVENTS) != 0) {
        SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
        exit(1);
    }
    if(TTF_Init()==-1)
    {
		SDL_Log("TTF_Init");
		exit(2);
	}
    // Create main-window, fullscreen
    
    main_window = SDL_CreateWindow(
        "Main Window",                  // window title
        SDL_WINDOWPOS_UNDEFINED,           // initial x position
        SDL_WINDOWPOS_UNDEFINED,           // initial y position
        800,                               // width, in pixels - ignored in fullscreen_desktop
        480,                               // height, in pixels - ignored in fullscreen_desktop
        SDL_WINDOW_FULLSCREEN_DESKTOP      // flags - see below
    );
    if (main_window == NULL) {
        // In the case that the window could not be made...
        printf("Could not create window: %s\n", SDL_GetError());
        exit(1);
    }
	SDL_Surface *main_surface = SDL_GetWindowSurface(main_window);
	
	return SDL_CreateRenderer(main_window, -1, SDL_RENDERER_ACCELERATED|SDL_RENDERER_PRESENTVSYNC);
}

void startPlaying(std::string path)
{
	int encoding;
	int encsize;
	long int freq;
	int channels;
	
	stopPlaying();
	SDL_memset(&want, 0, sizeof(want));
	
	if(path.rfind(".mp3") == path.length() - 4)
	{
		if(mpg123_init() != MPG123_OK)
		{
			SDL_Log("mpg123_Init() failed");
			exit(3);
		}
		int error;
		test = mpg123_new(mpg123_supported_decoders()[0], &error);
		mpg123_open_fixed(test, path.data(), 2, MPG123_ENC_FLOAT);

		mpg123_getformat2(test, &freq, &channels, &encoding, false);
		switch(encoding)
		{
			case MPG123_ENC_SIGNED_16:
				want.format = AUDIO_S16;
			break;
			case MPG123_ENC_UNSIGNED_16:
				want.format = AUDIO_U16;
			break;
			case MPG123_ENC_UNSIGNED_8:
				want.format = AUDIO_U8;
			break;
			case MPG123_ENC_SIGNED_8:
				want.format = AUDIO_S8;
			break;
			case MPG123_ENC_ULAW_8:
				SDL_Log("MPG123_ENC");
				exit(1);
			break;
			case MPG123_ENC_ALAW_8:
				SDL_Log("MPG123_ENC");
				exit(1);
			break;
			case MPG123_ENC_SIGNED_32:
				want.format = AUDIO_S32;
			break;
			case MPG123_ENC_UNSIGNED_32:
				SDL_Log("MPG123_ENC");
				exit(1);
			break;
			case MPG123_ENC_SIGNED_24:
				SDL_Log("MPG123_ENC");
				exit(1);
			break;
			case MPG123_ENC_UNSIGNED_24:
				SDL_Log("MPG123_ENC");
				exit(1);
			break;
			case MPG123_ENC_FLOAT_32:
				want.format = AUDIO_F32;
			break;
			case MPG123_ENC_FLOAT_64:
				SDL_Log("MPG123_ENC");
				exit(1);
			break;
			default:
				exit(1);
			break;
		}
		encsize = mpg123_encsize(encoding);
		buffer_length = mpg123_length(test) * encsize * channels;
		audio_buffer = new unsigned char[buffer_length];
		
		want.freq = (int) freq;
		want.channels = channels;
		

		//want.freq = 48000;
		//want.channels = 2;
		want.samples = 4096;
		want.callback = &AudioCallback;
		if (SDL_OpenAudio(&want, &have) < 0)
		{
			SDL_Log("Failed to open audio: %s", SDL_GetError());
		}
		else
		{
			std::cout << have.freq << "Hz " << (int) have.channels << " channels " << have.samples << " samples" << std::endl;
		}
		
		stop_thread = false;
		iterator = 0;
		mpg_decode = new std::thread(decodeMP3_thread);
		mpg_decode->detach();
	}
	else if(path.rfind(".ogg") == path.length() - 4 || path.rfind(".oga") == path.length() - 4 )
	{
		stop_thread = false;
		iterator = 0;
		my_path = path;
		ogg_decode = new std::thread(decodeOGG_thread, my_path.data());
		ogg_decode->detach();
	}

	if(!enqueue_next)
	{
	    std::map<std::string, SDL_Texture *>::iterator map_iterator = path_to_texture_map.find(path);
	    playlist.clear();
	    playlist.emplace_front("", NULL);
	    for(;map_iterator != path_to_texture_map.end(); map_iterator++)
	    {
	        std::string t(map_iterator->first);
	        if(t.rfind(".mp3") == t.length()-4
	        || t.rfind(".ogg") == t.length() - 4
	        || t.rfind(".oga") == t.length() - 4
	        || t.rfind(".mp3") == t.length() - 4)
	        {
	            playlist.emplace_back(t);
	        }
	    }
	}
	playlist.pop_front();
	playlist_iterator = playlist.begin();
	if(ogg_thread_deletable)
	{
	    ogg_thread_deletable = false;
	    delete ogg_decode;
	}
	if(mp3_thread_deletable)
	{
	    mp3_thread_deletable = false;
	    delete mpg_decode;
	}
}


void quitSDL()
{
	SDL_DestroyWindow(main_window);
    SDL_Quit();
}

SDL_Texture * JPGtoTextureWithRenderer(const char * file_to_load, SDL_Renderer * renderer)
{
	const int COLOR_COMPONENTS = 3;
    long unsigned int _jpegSize; //!< _jpegSize from above
	unsigned char* _compressedImage; //!< _compressedImage from above

//	const char * file_to_load = "test.jpg";
	struct stat file_stat;
	if(stat(file_to_load, &file_stat) !=0)
		return NULL;
	unsigned char * filebuffer = new unsigned char[file_stat.st_size];
	int load_fd = open(file_to_load, O_RDONLY);
	read(load_fd, filebuffer, file_stat.st_size);
	close(load_fd);
	
	_compressedImage = filebuffer;
	_jpegSize = file_stat.st_size;
	
	
	int jpegSubsamp, width, height;

	tjhandle _jpegDecompressor = tjInitDecompress();
	tjDecompressHeader2(_jpegDecompressor, _compressedImage, _jpegSize, &width, &height, &jpegSubsamp);
	unsigned char bg_buffer[width*height*COLOR_COMPONENTS]; //!< will contain the decompressed image
	tjDecompress2(_jpegDecompressor, _compressedImage, _jpegSize, bg_buffer, width, 0/*pitch*/, height, TJPF_RGB, TJFLAG_FASTDCT);
	tjDestroy(_jpegDecompressor);
	delete[] filebuffer;
	SDL_Surface * background = SDL_CreateRGBSurfaceWithFormatFrom((void*)bg_buffer, width, height, 24, 3*width, SDL_PIXELFORMAT_RGB24);
	SDL_Texture * backgroundTex = SDL_CreateTextureFromSurface(renderer, background);
	SDL_Texture * return_value = SDL_CreateTexture(renderer,
                                                   SDL_PIXELFORMAT_RGBA8888,
                                                   SDL_TEXTUREACCESS_TARGET,
                                                   current.h/2, current.h/2);
    SDL_SetRenderTarget(renderer, return_value);
    SDL_RenderClear(renderer);

    SDL_Rect dst_rect = { 0, 0, 0, 0};
    dst_rect.w = current.h/2;
    dst_rect.h = (int) current.h/2 *((double)height/(double)width);
    SDL_RenderCopy(renderer, backgroundTex, NULL, &dst_rect);
    SDL_SetRenderTarget(renderer, NULL);
    SDL_DestroyTexture(backgroundTex);
    SDL_FreeSurface(background);

	return return_value;
}

SDL_Texture * createCursor(SDL_Renderer * renderer)
{
	/* Create a 32-bit surface with the bytes of each pixel in R,G,B,A order,
       as expected by OpenGL for textures */
    SDL_Surface *surface;
    Uint32 rmask, gmask, bmask, amask;

    /* SDL interprets each pixel as a 32-bit number, so our masks must depend
       on the endianness (byte order) of the machine */
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    rmask = 0xff000000;
    gmask = 0x00ff0000;
    bmask = 0x0000ff00;
    amask = 0x000000ff;
#else
    rmask = 0x000000ff;
    gmask = 0x0000ff00;
    bmask = 0x00ff0000;
    amask = 0xff000000;
#endif

    surface = SDL_CreateRGBSurface(0, 100, 100, 32,
                                   rmask, gmask, bmask, amask);
    if (surface == NULL) {
        SDL_Log("SDL_CreateRGBSurface() failed: %s", SDL_GetError());
        exit(1);
    }
	/* Filling the surface with red color. */
	SDL_FillRect(surface, NULL, SDL_MapRGB(surface->format, 255, 0, 0));
	
	// play with renderer
    SDL_Texture *cursorTex = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);
    return cursorTex;
}

SDL_Texture * TextToTexture(const char * text, SDL_Renderer *renderer)
{
	TTF_Font *font;
	font=TTF_OpenFont("/usr/share/fonts/truetype/Carlito-Regular.ttf", current.h*4.0/72.0);
	if(!font)
	{
		SDL_Log("TTF_OpenFont");
		exit(2);
	}
	// add newlines to text
	/*std::string s(text);
	for (int i = 10; i < s.length(); i+= 10)
	{
	    s.insert(i, "\n");
	}*/
	SDL_Color color={255,255,255, 0};
	SDL_Surface *text_surface;
	if(!(text_surface=TTF_RenderUTF8_Blended_Wrapped(font,text,color, current.h/2)))
	{
		//handle error here, perhaps print TTF_GetError at least
		SDL_Log("TTF_RenderUTF8_Solid");
		exit(2);
	}
	SDL_Texture * backgroundTex = SDL_CreateTextureFromSurface(renderer, text_surface);
	SDL_FreeSurface(text_surface);
		
	TTF_CloseFont(font);
	font=NULL;
	return backgroundTex;
}

void switchToPath(std::string path)
{
	if(path.rfind("/.") == path.length()-2)
		return;
	for(auto t : path_to_texture_map)
	{
		SDL_DestroyTexture(t.second);
	}
	path_to_texture_map.clear();
	DIR * directory = opendir(path.data());
	if(directory == NULL)
	{
		SDL_Log("Could not open directory!");
		exit(1);
	}
	struct dirent * dir_entry = readdir(directory);
	while(dir_entry !=NULL)
	{
                std::string dir_string(dir_entry->d_name);
		bool is_dir = (dir_entry->d_type == DT_DIR);
		std::string dirname(dir_entry->d_name);
		if(skip_set.count(dir_string) == 0)
		{
			SDL_Texture * newTex = JPGtoTextureWithRenderer((path + dirname + "/Folder.jpg").data(), renderer);
			if(newTex!= NULL)
			{
				path_to_texture_map[path + dirname + "/"] = newTex;
			}
			else
			{
				newTex = JPGtoTextureWithRenderer((path + dirname + "/AlbumArtSmall.jpg").data(), renderer);
				if(newTex!= NULL)
				{
				path_to_texture_map[path + dirname + "/"] = newTex;
				}
				else
				{
					if(dirname == "..")
					{
						newTex = TextToTexture("Verzeichnis aufwärts", renderer);
					}
					else if(dirname == ".")
					{
						newTex = TextToTexture("Dieses Verzeichnis", renderer);
					}
					else
					{
						newTex = TextToTexture(dirname.data(), renderer);
					}
					if(is_dir)
					{
						path_to_texture_map[path + dirname + "/"] = newTex;
					}
					else
					{
						path_to_texture_map[path + dirname] = newTex;
					}
				}

			}
		}
		dir_entry = readdir(directory);
	}
	closedir(directory);
}

int main(int argc, char* argv[])
{
	// hacking environmant
/*	const char * hack_to_alsa = "SDL_AUDIODRIVER=alsa";
	putenv((char *)hack_to_alsa);
	*/

	renderer = initSDL();
	
	// fetch display-resolution
    if(SDL_GetCurrentDisplayMode(0, &current) != 0)
    {
      // In case of error...
      SDL_Log("Could not get display mode for video display #%d: %s", 0, SDL_GetError());
      exit(1);
	}
	
    
    
    SDL_Texture * cursorTex = createCursor(renderer);
    
    if(argc > 1)
    {
		starting_path = std::string(argv[1]);
		if(starting_path.rfind("/") != starting_path.length())
			starting_path += "/";
	}
	else
	{
		starting_path = std::string("/home/andreas/Musik/");
	}
	std::string stophere_string = starting_path.substr(0, 1+starting_path.rfind('/', starting_path.length()-2));
	std::cout << stophere_string << std::endl;
	switchToPath(starting_path);
    
    SDL_Rect cursor_target {0, 0, 10, 10};
    
    SDL_Rect background_target {0, 0, 0, 0};
    Uint32 last_activity = SDL_GetTicks();
    int last_pic_x = 0;
    int last_push_x = 0;
    int last_push_y = 0;
    int speed = 0;
    int old_xs[3] = {0, 0, 0};
    int xs_index = 0;
	bool cont = true;
    while (cont)
    {
		if(enqueue_next)
		{
			playlist_iterator++;
			if(playlist_iterator != playlist.end())
			{
			    stopPlaying();
				startPlaying(*playlist_iterator);
			}
			else
			{
				stopPlaying();
			}
			enqueue_next = false;
		}
		bool clicked = false;
        SDL_Event e;
        int old_x = cursor_target.x;

        while (SDL_PollEvent(&e))
        {
			SDL_MouseMotionEvent ev = e.motion;
			SDL_TouchFingerEvent tev = e.tfinger;
            switch(e.type)
            {
				case SDL_QUIT:
					cont = false;
                break;
                case SDL_MOUSEMOTION:
					cursor_target.x = ev.x;
					cursor_target.y = ev.y;
					last_activity = SDL_GetTicks();
					if(ev.state & SDL_BUTTON_LMASK)
					{
						background_target.x = cursor_target.x - last_pic_x;
					}
				    break;
                case SDL_FINGERMOTION:
                    cursor_target.x = tev.x * current.w;
                    cursor_target.y = tev.y * current.h;
                    last_activity = SDL_GetTicks();
                    xs_index++;
                    xs_index %= 3;
                    old_xs[xs_index] = cursor_target.x - old_x;
                    /*if(true)
                    {
                        background_target.x = cursor_target.x - last_pic_x;
                    }*/
                    break;
                case SDL_FINGERUP:
                    //speed = current.w * tev.x - old_x;
                    speed = 0;
                    for(int i = 0; i < 3; i++)
                    {
                        if(i != xs_index || old_xs[xs_index] != 0)
                        {
                            speed += old_xs[i];
                        }
                    }
                    if(old_xs[xs_index] != 0)
                    {
                        speed /= 3;
                    }
                    else
                    {
                        speed /= 2;
                    }
                    last_pic_x = cursor_target.x - background_target.x;
                    if(abs(last_push_x - current.w * tev.x) < 13 && abs(last_push_y - current.h * tev.y) < 13)
                        clicked = true;
                    break;
				case SDL_MOUSEBUTTONUP:
                    speed = ev.x - old_x;
                    last_pic_x = cursor_target.x - background_target.x;
                    if(abs(last_push_x - ev.x) < 13 && abs(last_push_y - ev.y) < 13)
                        clicked = true;
				break;
                case SDL_FINGERDOWN:
                    speed = 0;
                    std::memset(old_xs, 0, sizeof(old_xs));
                    last_push_x = tev.x * current.w;
                    last_push_y = tev.y * current.h;
                    last_pic_x = cursor_target.x - background_target.x;
                    /*
                    if(cursor_target.x > 0.95*current.w && cursor_target.y < 0.05*current.h) // TODO: define exit-procedure clearly
                        cont = false;
                    if(cursor_target.x < 0.05*current.w && cursor_target.y < 0.05*current.h)
                    {
                        SDL_AudioStatus status;
                        status = SDL_GetAudioStatus();
                        switch(status)
                        {
                            case SDL_AUDIO_STOPPED:
                                break;
                            case SDL_AUDIO_PAUSED:
                                SDL_PauseAudio(0);
                                break;
                                case SDL_AUDIO_PLAYING:
                                    SDL_PauseAudio(1);
                                    break;
                        }
                    }
                     */
                    break;
				case SDL_MOUSEBUTTONDOWN:
                    speed = 0;
                    last_push_x = ev.x;
                    last_push_y = ev.y;
                    last_pic_x = cursor_target.x - background_target.x;
                    if(cursor_target.x > 0.95*current.w && cursor_target.y < 0.05*current.h) // TODO: define exit-procedure clearly
                        cont = false;
                    if(cursor_target.x < 0.05*current.w && cursor_target.y < 0.05*current.h)
                    {
                        SDL_AudioStatus status;
                        status = SDL_GetAudioStatus();
                        switch(status)
                        {
                            case SDL_AUDIO_STOPPED:
                                break;
                            case SDL_AUDIO_PAUSED:
                                SDL_PauseAudio(0);
                                break;
                                case SDL_AUDIO_PLAYING:
                                    SDL_PauseAudio(1);
                                    break;
                        }
                    }
				break;
            }
        }
        if( background_target.x < (-1.0) *path_to_texture_map.size() * (current.h*0.5 + 20) - (current.h*0.5 + 20)/2)
        {
            background_target.x = (-1.0)*path_to_texture_map.size() * (current.h*0.5 + 20) - (current.h*0.5 + 20)/2;
            speed = 0;
        }
        else if(background_target.x > (current.h*0.5 + 20)/2)
        {
            background_target.x = (current.h*0.5 + 20)/2;
            speed = 0;
        }
        background_target.x += speed;
        speed *= 0.92;

        SDL_RenderClear(renderer);
        int displacement = 0;
        for(auto icon_descr : path_to_texture_map)
        {
			displacement += current.h*0.5 + 20;
			if(background_target.x + displacement < -current.h*0.5-5 || background_target.x + displacement > current.w)
				continue;
			int w, h;
			SDL_QueryTexture(icon_descr.second, NULL, NULL, &w, &h);
			SDL_Rect rect = {
				background_target.x + displacement,
				current.h/2 - h/2,
				current.h/2,
				current.h/2 *((double)h/(double)w)};
			if(SDL_RenderCopy(renderer, icon_descr.second, NULL, &rect) != 0)
			{
				rect.w -= rect.x;
				rect.x = 0;
				SDL_RenderCopy(renderer, icon_descr.second, NULL, &rect);
			}
			if(clicked)
			{
				if(last_push_x > rect.x && last_push_x < rect.x+rect.w
				&& last_push_y > rect.y && last_push_y < rect.y+rect.h)
				{
					std::string t(icon_descr.first);
					int index = t.rfind("/../");
					if((index != std::string::npos) )
					{
						int index1 = t.rfind("/", index-1);
						t = t.substr(0, index1+1);
					}
					index = t.rfind("/./");
					if((index == t.length() - 3) )
					{
						t = t.substr(0, index+1);
					}
					if(t.rfind(".mp3") == t.length()-4 || t.rfind(".ogg") == t.length() - 4 || t.rfind(".oga") == t.length() - 4 )
					{
						startPlaying(t);
					}
					else
					{
					    if(t != stophere_string && t.rfind('/') == t.length()-1)
					    {
					        switchToPath(t);
					        displacement=0;
					        background_target.x = 0;
					        break;
					    }
					}
				}
			}
		}
        if(last_activity + 750 > SDL_GetTicks() )
        {
			SDL_RenderCopy(renderer, cursorTex, NULL, &cursor_target);
		}
        /*if(background_target.x < (-1) * displacement-current.h/2)
        {
            background_target.x = displacement - current.w/2;
            speed = 0;
        }
        else if(background_target.x > displacement)
        {
            std::cout << background_target.x << std::endl;
            background_target.x = displacement - current.h/2;
            speed = 0;
        }*/
		SDL_Rect degree = {(current.w-10) * percent_done, current.h - 10, 10, 10};
		SDL_RenderCopy(renderer, cursorTex, NULL, &degree);
        SDL_RenderPresent(renderer);
    }
	
	// cleanup
	SDL_DestroyTexture(cursorTex);
	for(auto icon_descr : path_to_texture_map)
	{
		SDL_DestroyTexture(icon_descr.second);
	}
	SDL_DestroyRenderer(renderer);
	stopPlaying();
	quitSDL();

    return 0;
}
